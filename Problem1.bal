import ballerina/io;
import ballerina/grpc as grpc;

type add_new_ln record {
    
};

type LearnerClient record {
    add_new_ln;
    
};

LearnerClient ep = check new ("http://localhost:8080");

type AddLnReq record {
    
};

type AddLnRes record {
    
};

type ShowAllWithCritReq record {
    
};

type Show_all_with_criteriaStreamingClient record {
    
};

type ShowAllLnRes record {
    
};

type ShowAllLnsReq record {
    
};

type ShowLnRes record {
    
};

type ShowLnReq record {
    
};

type ShowAllWithCritRes record {
    
};

type DeleteLnRes record {
    
};

type DeleteLnReq record {
    
};

public function main() {
    io:print();
    AddLnReq addLnReq = {
        fn: {
            name: "Rosa",
            versionNum: 1,
            lang: "Help",
            fnDef: "print('hello world!')",
            keywords: ["beginner"],
            devName: "test",
            devEmail: "test@test"
        }
    };
    AddLnRes|grpc:Error addLnRes = ep->add_new_ln(addLnReq);
    if addLnRes is error {
        io:println("Error adding new ln: ", addLnRes.message());
    } else {
        io:println("Success: ", addLnRes.message);
    }

    AddLnReq[] addLnReq = [
        { // should succeed
        Ln: {
            devEmail: "Teri2TT",
            keywords: ["testing", "test"],
            versionNum: 1,
            name: "test1",
            lang: "test1",
            devName: "test1",
            fnDef: "test1"
        }
    }, 
        { // should fail
    Ln: {
            devEmail: "test@test",
            keywords: ["testing", "test"],
            versionNum: 1,
            name: "test1",
            lang: "test1",
            devName: "test1",
            fnDef: "test1"
        }
    }, 
        { // should succeed
        Ln: {
            devEmail: "test@test",
            keywords: ["testing", "test"],
            versionNum: 1,
            name: "test2",
            lang: "test2",
            devName: "test2",
            fnDef: "test2"
        }
    }
    ];
    Add_LnStreamingClient|grpc:Error addLnStream = ep->add_Ln();
    if addLnStream is error {
        io:println("error added fns: ", addLnStream.message());
    } else {
        foreach AddLnReq aLReq in addLnReq {
            grpc:Error? err = addLnStream->sendAddLnReq(aLReq);
            if err is error {
                io:println("Failed to send addFns req");
            }
        }

        grpc:Error? err = addLnStream->complete();
        if err is error {
            io:println("Failed to send complete message");
        } else {
            AddLnRes|grpc:Error? LnRes = addLnStream->receiveAddLnRes();
            if LnRes is error {
                io:println("Failed to retrieve addLnRes: ", LnRes.message());
            } else {
                if LnRes is AddLnRes {
                    foreach string msg in LnRes.funcNames {
                        io:println(msg);
                    }
                }
            }
        }

    }

    ShowLnReq showLnReq = {
        funcName: "HelpMePlease",
        versionNum: 1
    };
    ShowLnRes|grpc:Error showLnRes = ep->show_Ln(showLnReq);
    if showLnRes is error {
        io:println("Error retrieving Ln: ", showLnRes.message());
    } else {
        io:println("Success: ", showLnRes);
    }

    ShowAllLnsReq showAllLnsReq = {
        funcName: "LostMeThere"
    };
    stream<ShowAllLnRes, grpc:Error?>|grpc:Error showAllLnStream = ep->show_all_Ln(showAllLnReq);
    if showAllLnStream is error {
        io:println("Failed to show all Ln: ", showAllLnStream.message());
    } else {
        error? e = showAllLnStream.forEach(function(ShowAllLnRes res) {
            io:println("Successfully retrieved Ln: ", res.Ln.name);
        });
    }

    ShowAllWithCritReq[] showAllWithCritReqs = [{
        keywords: ["Help", "Me"],
        lang: ""
    }];
    Show_all_with_criteriaStreamingClient|grpc:Error showAllWithCritStream = ep->show_all_with_criteria();
    if showAllWithCritStream is error {
        io:println("error occured setting up showAllWithCrit stream: ", showAllWithCritStream.message());
    } else {
        foreach ShowAllWithCritReq showAllWithCritReq in showAllWithCritReqs {
            error? err = showAllWithCritStream->sendShowAllWithCritReq(showAllWithCritReq);
            if err is error {
                io:println("failed to send showAllWithCriteria req: ", err.message());
            }
        }
        error? err = showAllWithCritStream->done();
        if err is error {
            io:println("failed to send showAllWithCriteria complete message: ", err.message());
        }

        ShowAllWithCritRes|grpc:Error? showAllWithCritRes = showAllWithCritStream->receiveShowAllWithCritRes();
        while true {
            if showAllWithCritRes is error {
                io:println("failed to send showAllWithCriteria complete message: ", showAllWithCritRes.message());
                break;
            } else {
                if showAllWithCritRes is () {
                    break;
                } else {
                    io:println("showAllWithCriteria response: ", showAllWithCritRes.fns);
                    showAllWithCritRes = showAllWithCritStream->receiveShowAllWithCritRes();
                }
            }
        }
    }

    DeleteLnReq delLnReq = {
        funcName: "HelpMePlease",
        versionNum: 1
    };
    DeleteLnRes|grpc:Error delRes = ep->delete_Ln(delFnReq);
    if delRes is error {
        io:println("Error deleting Ln: ", delRes.message());
    } else {
        io:println("Success: ", delRes.message);
    }
}